CARGO = cargo

DESTDIR =
PREFIX = /usr/local
TARGET = release
TARGET_OPT = --release

PKG_CONFIG != command -v pkgconf || command -v pkg-config
COMPDIR_BASH != $(PKG_CONFIG) --define-variable=prefix="$(PREFIX)" \
  --variable=completionsdir bash-completion 2> /dev/null \
  || echo "$(PREFIX)/share/bash-completion/completions"
COMPDIR_FISH != $(PKG_CONFIG) \
  --variable=completionsdir fish 2> /dev/null \
  || echo "$(PREFIX)/share/fish/vendor_completions.d"
COMPDIR_ZSH = $(PREFIX)/zsh/site-functions

appname = argon2-cli

all:
	$(CARGO) build $(TARGET_OPT)
	$(CARGO) run $(TARGET_OPT) --bin generate-completions -- target/completions

check:
	env RUST_BACKTRACE=1 $(CARGO) test $(TARGET_OPT)

install: install-completions
	install -D target/$(TARGET)/$(appname) $(DESTDIR)$(PREFIX)/bin/$(appname)

install-completions:
	install -D target/completions/_$(appname) $(DESTDIR)$(COMPDIR_ZSH)/_$(appname)
	install -D target/completions/$(appname).bash $(DESTDIR)$(COMPDIR_BASH)/$(appname)
	install -D target/completions/$(appname).fish $(DESTDIR)$(COMPDIR_FISH)/$(appname).fish

.PHONY: all check install install-completions
