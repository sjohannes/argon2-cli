// argon2-cli
// Copyright (C) 2019  Johannes Sasongko <sasongko@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use clap::*;

pub fn build_cli<'a, 'b>() -> App<'a, 'b> {
    App::new("argon2-cli")
        .version(crate_version!())
        .about(crate_description!())
        .after_help(concat!(
            "STANDARD INPUT:\n",
            "    The password to hash or verify\n\n",
            "RETURN CODES:\n",
            "    0    Success\n",
            "    1    Error\n",
            "    2    Verification failure",
        ))
        .help_message("Print help information")
        .version_message("Print version information")
        .set_term_width(80)
        .args(&[
            Arg::with_name("ARG").required(true).help(concat!(
                "With --verify: the hash to verify against; ",
                "with --random-hash: the salt length; ",
                "otherwise: the salt to hash with",
            )),
            Arg::with_name("-i")
                .short("i")
                .help("Use Argon2i [default]"),
            Arg::with_name("-d")
                .short("d")
                .help("Use Argon2d, or Argon2id if -i is also specified"),
            Arg::with_name("-t")
                .short("t")
                .value_name("ITER")
                .default_value("3")
                .help("Number of iterations"),
            Arg::with_name("-m")
                .short("m")
                .value_name("MEM")
                .conflicts_with("-k")
                .help("Memory usage (2^MEM KiB)"),
            Arg::with_name("-k")
                .short("k")
                .value_name("MEM")
                .default_value("4096")
                .help("Memory usage (KiB)"),
            Arg::with_name("-p")
                .short("p")
                .value_name("THREADS")
                .default_value("1")
                .help("Number of threads"),
            Arg::with_name("-l")
                .short("l")
                .value_name("LEN")
                .default_value("32")
                .help("Hash length"),
            Arg::with_name("-e")
                .short("e")
                .help("Show encoded hash only"),
            Arg::with_name("-r")
                .short("r")
                .conflicts_with("-e")
                .help("Show raw hash only"),
            Arg::with_name("-v")
                .short("v")
                .value_name("VER")
                .possible_values(&["10", "13"])
                .default_value("13")
                .help("Argon2 version"),
            Arg::with_name("--random-salt")
                .long("random-salt")
                .conflicts_with("-r")
                .help("Use a random salt of length ARG"),
            Arg::with_name("--verify")
                .long("verify")
                .conflicts_with_all(&[
                    "-i",
                    "-d",
                    "-t",
                    "-m",
                    "-k",
                    "-p",
                    "-l",
                    "-e",
                    "-r",
                    "-v",
                    "--random-salt",
                ])
                .help("Verify a password against the hash specified in ARG"),
        ])
}

#[test]
fn conflicts() {
    let mut parser = build_cli();
    let result = parser.get_matches_from_safe_borrow(&["", "-m", "11", "-k", "2048", "abcdefgh"]);
    assert_eq!(ErrorKind::ArgumentConflict, result.unwrap_err().kind);
    let result = parser.get_matches_from_safe_borrow(&["", "-e", "-r", "abcdefgh"]);
    assert_eq!(ErrorKind::ArgumentConflict, result.unwrap_err().kind);
}
