// argon2-cli
// Copyright (C) 2019  Johannes Sasongko <sasongko@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

mod cli;

use argon2::{Config, Variant};

#[repr(i32)]
enum ExitCode {
    Success = 0,
    Error = 1,
    VerificationFailed = 2,
}

impl Into<i32> for ExitCode {
    fn into(self) -> i32 {
        self as i32
    }
}

struct FullHashOutput<'a> {
    encoded: String,
    raw: String,
    time: std::time::Duration,
    config: Config<'a>,
}

enum Success<'a> {
    /// Hash output: full
    HashFull(FullHashOutput<'a>),
    /// Hash output: encoded only
    HashEncoded(String),
    /// Hash output: raw only
    HashRaw(String),
    /// Hash verification succeeded
    Verified,
}

enum Error {
    /// Argon2 hashing error
    HashingFailed(argon2::Error),
    /// Invalid value for an argument
    InvalidArgumentValue(&'static str),
    /// Attempted to verify password using invalid hash
    InvalidHash,
    /// Error while reading from stdin
    StdinReadFailed,
    /// Password verification failed
    VerificationFailed,
}

fn get_random_salt(length: usize, rng: &mut impl rand::RngCore) -> Vec<u8> {
    let mut salt = vec![0; length];
    rng.fill_bytes(&mut salt);
    salt
}

trait ArgMatchesExt {
    fn get_memory_kib(&self) -> Result<u32, Error>;
    fn get_salt(&self) -> Result<std::borrow::Cow<[u8]>, Error>;
    fn get_variant(&self) -> Variant;
    fn parse_value_of<T: std::str::FromStr>(&self, name: &'static str) -> Result<T, Error>;
    fn value_of_bytes(&self, name: &str) -> Option<&[u8]>;
}

impl ArgMatchesExt for clap::ArgMatches<'_> {
    fn get_memory_kib(&self) -> Result<u32, Error> {
        if self.is_present("-m") {
            let m = self.parse_value_of("-m")?;
            Ok(1u32.checked_shl(m).unwrap_or(u32::max_value()))
        } else {
            self.parse_value_of("-k")
        }
    }

    fn get_salt(&self) -> Result<std::borrow::Cow<[u8]>, Error> {
        Ok(if self.is_present("--random-salt") {
            get_random_salt(self.parse_value_of("ARG")?, &mut rand::rngs::OsRng).into()
        } else {
            self.value_of_bytes("ARG").unwrap().into()
        })
    }

    fn get_variant(&self) -> Variant {
        match (self.is_present("-i"), self.is_present("-d")) {
            (false, false) => Variant::Argon2i, // Default
            (true, false) => Variant::Argon2i,
            (false, true) => Variant::Argon2d,
            (true, true) => Variant::Argon2id,
        }
    }

    fn parse_value_of<T: std::str::FromStr>(&self, name: &'static str) -> Result<T, Error> {
        self.value_of(name)
            .ok_or(Error::InvalidArgumentValue(name))
            .and_then(|s| s.parse().map_err(|_| Error::InvalidArgumentValue(name)))
    }

    fn value_of_bytes(&self, name: &str) -> Option<&[u8]> {
        #[cfg(unix)]
        {
            use std::os::unix::ffi::OsStrExt;
            self.value_of_os(name).map(|s| s.as_bytes())
        }

        #[cfg(not(unix))]
        {
            self.value_of(name).map(|s| s.as_bytes())
        }
    }
}

fn slurp(reader: &mut impl std::io::Read) -> std::io::Result<Vec<u8>> {
    let mut buf = Vec::new();
    reader.read_to_end(&mut buf).map(|_| buf)
}

pub fn encode_hash(config: &Config, salt: &[u8], hash: &[u8]) -> String {
    // Based on rust-argon2's encoding::encode_string (private function)
    format!(
        "${}$v={}$m={},t={},p={}${}${}",
        config.variant,
        config.version,
        config.mem_cost,
        config.time_cost,
        config.lanes,
        base64::encode_config(salt, base64::STANDARD_NO_PAD),
        base64::encode_config(hash, base64::STANDARD_NO_PAD),
    )
}

fn process_input<'a>(
    args: clap::ArgMatches,
    stdin: &mut impl std::io::Read,
) -> Result<Success<'a>, Error> {
    if args.is_present("--verify") {
        let pwd = slurp(stdin).map_err(|_| Error::StdinReadFailed)?;
        let hash = args
            .value_of("ARG")
            .ok_or(Error::InvalidArgumentValue("ARG"))?;
        return match argon2::verify_encoded(&hash, &pwd) {
            Ok(true) => Ok(Success::Verified),
            Ok(false) => Err(Error::VerificationFailed),
            _ => Err(Error::InvalidHash),
        };
    }

    let threads = args.parse_value_of("-p")?;
    let config = Config {
        ad: &[],
        hash_length: args.parse_value_of("-l")?,
        lanes: threads,
        mem_cost: args.get_memory_kib()?,
        secret: &[],
        thread_mode: argon2::ThreadMode::from_threads(threads),
        time_cost: args.parse_value_of("-t")?,
        variant: args.get_variant(),
        version: match args.value_of("-v").unwrap() {
            "13" => argon2::Version::Version13,
            "10" => argon2::Version::Version10,
            _ => return Err(Error::InvalidArgumentValue("-v")),
        },
    };
    let pwd = slurp(stdin).or_else(|_| Err(Error::StdinReadFailed))?;
    let salt = args.get_salt()?;

    let wants_raw = args.is_present("-r");
    let wants_encoded = args.is_present("-e");
    debug_assert!(!(wants_raw && wants_encoded)); // Can't have both
    let wants_full = !wants_raw && !wants_encoded;
    let time_start = if wants_full {
        Some(std::time::Instant::now())
    } else {
        None
    };

    let hash = argon2::hash_raw(&pwd, &salt, &config).map_err(Error::HashingFailed)?;

    let time = if wants_full {
        Some(time_start.unwrap().elapsed())
    } else {
        None
    };

    if wants_raw {
        debug_assert!(!args.is_present("-e"));
        Ok(Success::HashRaw(hex::encode(&hash)))
    } else {
        let encoded = encode_hash(&config, &salt, &hash);
        if wants_encoded {
            Ok(Success::HashEncoded(encoded))
        } else {
            argon2::verify_raw(&pwd, &salt, &hash, &config).unwrap();
            Ok(Success::HashFull(FullHashOutput {
                encoded,
                raw: hex::encode(&hash),
                time: time.unwrap(),
                config,
            }))
        }
    }
}

fn process_result(
    result: &Result<Success<'_>, Error>,
    stdout: &mut impl std::io::Write,
    stderr: &mut impl std::io::Write,
) -> std::io::Result<ExitCode> {
    let exit_code = match result {
        Ok(Success::HashFull(h)) => {
            writeln!(
                stdout,
                concat!(
                    "Type:           {}\n",
                    "Iterations:     {}\n",
                    "Memory:         {} KiB\n",
                    "Parallelism:    {}\n",
                    "Hash:           {}\n",
                    "Encoded:        {}\n",
                    "{} seconds\n",
                    "Verification ok"
                ),
                h.config.variant.as_uppercase_str(),
                h.config.time_cost,
                h.config.mem_cost,
                h.config.lanes,
                h.raw,
                h.encoded,
                h.time.as_secs_f32(),
            )?;
            ExitCode::Success
        }
        Ok(Success::HashEncoded(h)) | Ok(Success::HashRaw(h)) => {
            writeln!(stdout, "{}", h)?;
            ExitCode::Success
        }
        Ok(Success::Verified) => {
            let _ = writeln!(stdout, "OK"); // When verifying, ignore IO error
            ExitCode::Success
        }
        Err(Error::HashingFailed(e)) => {
            writeln!(stderr, "Error: {}", e)?;
            ExitCode::Error
        }
        Err(Error::InvalidArgumentValue(arg)) => {
            writeln!(stderr, "Error: Invalid value for {}", arg)?;
            ExitCode::Error
        }
        Err(Error::InvalidHash) => {
            writeln!(stderr, "Error: Invalid hash")?;
            ExitCode::Error
        }
        Err(Error::StdinReadFailed) => {
            writeln!(stderr, "Error: Failed reading from stdin")?;
            ExitCode::Error
        }
        Err(Error::VerificationFailed) => {
            let _ = writeln!(stdout, "NOK"); // When verifying, ignore IO error
            ExitCode::VerificationFailed
        }
    };
    Ok(exit_code)
}

fn main() {
    let parser = cli::build_cli();
    let args = parser.get_matches();
    let result = process_input(args, &mut std::io::stdin());
    let exit_code = process_result(&result, &mut std::io::stdout(), &mut std::io::stderr())
        .unwrap_or(ExitCode::Error);
    std::process::exit(exit_code.into());
}

#[test]
fn args_memory() {
    let mem = cli::build_cli()
        .get_matches_from(&["", "-m", "11", "abcdefgh"])
        .get_memory_kib()
        .ok()
        .unwrap();
    assert_eq!(2048, mem);
}
