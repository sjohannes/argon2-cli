// argon2-cli
// Copyright (C) 2019  Johannes Sasongko <sasongko@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

mod cli;

fn main() -> Result<(), String> {
    let mut app = cli::build_cli();
    let bin_name = app.get_name().to_owned();
    let dir = std::env::args_os().nth(1).unwrap_or_else(|| ".".into());
    std::fs::create_dir_all(&dir).map_err(|e| e.to_string())?;
    for shell in clap::Shell::variants().iter() {
        use std::str::FromStr;
        let shell = clap::Shell::from_str(shell)?;
        app.gen_completions(&bin_name, shell, &dir);
    }
    Ok(())
}
