# argon2-cli

Drop-in replacement for the [command-line utility](https://github.com/P-H-C/phc-winner-argon2#command-line-utility) provided by the Argon2 reference implementation, with some extra features:

* Verification
* Random salt

The Argon2 implementation used is [rust-argon2](https://github.com/sru-systems/rust-argon2).

Currently verification is only supported for encoded hashes (those starting with `$argon2`), not for raw hashes.
