use std::process::{Command, Stdio};

pub fn command() -> Command {
    let mut cmd = Command::new(env!("CARGO_BIN_EXE_argon2-cli"));
    cmd.current_dir(std::env::current_exe().unwrap().parent().unwrap())
        .stdin(Stdio::piped())
        .stdout(Stdio::piped())
        .stderr(Stdio::piped());
    cmd
}

pub fn finish(cmd: &mut Command, input: &str) -> (i32, String, String) {
    use std::io::Write;

    let mut child = cmd.spawn().unwrap();
    let stdin = child.stdin.as_mut().unwrap();
    stdin.write_all(input.as_bytes()).unwrap();
    let result = child.wait_with_output().unwrap();

    let out = String::from_utf8_lossy(&result.stdout).to_string();
    let err = String::from_utf8_lossy(&result.stderr).to_string();
    print!("{}", out);
    eprint!("{}", err);

    (
        result.status.code().expect("terminated by signal"),
        out,
        err,
    )
}

macro_rules! run {
    ($input:tt, $($arg:tt)*) => {
        util::finish(util::command().args(&[$($arg)*]), $input)
    };
}
