#[macro_use]
mod util;

#[test]
fn empty() {
    let (ret, ..) = run!("", "");
    assert_eq!(1, ret);
}

#[test]
fn hash_encoded() {
    let (ret, out, _) = run!("xyz", "-e", "abcdefgh");
    assert_eq!(0, ret);
    assert_eq!(
        "$argon2i$v=19$m=4096,t=3,p=1$YWJjZGVmZ2g$0Adlkm7iCzwYSnrv64UNeiE5aACOjyg8+idfl+xzI0g\n",
        out,
    );
}

#[test]
fn hash_full() {
    let (ret, out, _) = run!("xyz", "abcdefgh");
    assert_eq!(0, ret);

    let mut split = out.split("\n");
    assert_eq!("Type:           Argon2i", split.next().unwrap());
    assert_eq!("Iterations:     3", split.next().unwrap());
    assert_eq!("Memory:         4096 KiB", split.next().unwrap());
    assert_eq!("Parallelism:    1", split.next().unwrap());
    assert_eq!(
        "Hash:           d00765926ee20b3c184a7aefeb850d7a213968008e8f283cfa275f97ec732348",
        split.next().unwrap(),
    );
    assert_eq!(
        "Encoded:        $argon2i$v=19$m=4096,t=3,p=1$YWJjZGVmZ2g$0Adlkm7iCzwYSnrv64UNeiE5aACOjyg8+idfl+xzI0g",
        split.next().unwrap()
    );
    assert!(split.next().unwrap().ends_with(" seconds"));
    assert_eq!("Verification ok", split.next().unwrap());
    assert_eq!("", split.next().unwrap());
    assert_eq!(None, split.next());
}

#[test]
fn hash_raw() {
    let (ret, out, _) = run!("xyz", "-r", "abcdefgh");
    assert!(ret == 0);
    assert_eq!(
        "d00765926ee20b3c184a7aefeb850d7a213968008e8f283cfa275f97ec732348\n",
        out,
    );
}

#[test]
fn help() {
    let (ret, ..) = run!("", "--help");
    assert_eq!(0, ret);
}

#[test]
fn invalid_arg() {
    let (ret, ..) = run!("", "--abcd");
    assert_eq!(1, ret);
}

#[test]
fn verify() {
    let (ret, ..) = run!(
        "xyz",
        "--verify",
        "$argon2i$v=19$m=4096,t=3,p=1$YWJjZGVmZ2g$0Adlkm7iCzwYSnrv64UNeiE5aACOjyg8+idfl+xzI0g",
    );
    assert_eq!(0, ret);
    let (ret, ..) = run!(
        "zzz",
        "--verify",
        "$argon2i$v=19$m=4096,t=3,p=1$YWJjZGVmZ2g$0Adlkm7iCzwYSnrv64UNeiE5aACOjyg8+idfl+xzI0g",
    );
    assert_eq!(2, ret);
}

#[test]
fn version() {
    let (ret, ..) = run!("", "--version");
    assert_eq!(0, ret);
}
